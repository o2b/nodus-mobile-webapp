var nodusApp = angular.module('nodusApp', ['ngRoute','ngMaterial','ngResource','ngMessages','ngMap','ngCookies','ngSanitize'])
.config(function($mdThemingProvider){
	$mdThemingProvider.definePalette('nodus', {
		'50': 'e3ebee',
		'100': 'b9ccd6',
		'200': '8aabba',
		'300': '5b899e',
		'400': '376f8a',
		'500': '145675',
		'600': '124f6d',
		'700': '0e4562',
		'800': '0b3c58',
		'900': '062b45',
		'A100': '7bc2ff',
		'A200': '48aaff',
		'A400': '1592ff',
		'A700': '0086fa',
		'contrastDefaultColor': 'light',
		'contrastDarkColors': [
			'50',
			'100',
			'200',
			'A100',
			'A200'
		], 'contrastLightColors': [
			'300',
			'400',
			'500',
			'600',
			'700',
			'800',
			'900',
			'A400',
			'A700'
		]
	});
	$mdThemingProvider
			.theme('default')
			.primaryPalette('nodus');
}).config(function($locationProvider, $routeProvider){
//	$locationProvider.hashPrefix('!');
	$routeProvider
			  .when("/dashboard", {
				templateUrl: './template-page/dashboard.html?'+$.now()
			}).when("/tickets", {
				templateUrl: './template-page/tickets.html?'+$.now()
			}).when("/itens", {
				templateUrl: './template-page/itens.html?'+$.now()
			}).when("/mapa", {
				templateUrl: './template-page/mapa.html?'+$.now()
			}).when("/sobre", {
				templateUrl: './template-page/sobre.html?'+$.now()
			});
}).config(function($mdToastProvider){
	$mdToastProvider
		.addPreset('loading', {
			argOption: 'textContent',
			methods: ['textContent'],
			options: function() {
				return {
					template:
						'<md-toast>' +
							'<div class="md-toast-content">' +
								'<md-progress-circular class="md-warn" md-diameter="20px" style="margin-right:10px;"></md-progress-circular> ' +
								'<span ng-bind="toast.textContent"></span>' +
							'</div>' +
						'</md-toast>',
					hideDelay: 0,
					controllerAs: 'toast',
					bindToController: true,
					position: 'bottom right',
					controller: 'defaultToastController'
				};
			}
		}).addPreset('error', {
			argOption: 'textContent',
			methods: ['textContent'],
			options: function() {
				return {
					template:
						'<md-toast>' +
							'<div class="md-toast-content">' +
								'<md-icon style="color:#fff;margin-right:6px;">warning</md-icon> ' +
								'<span ng-bind="toast.textContent"></span> ' +
								'<md-button class="md-highlight" ng-click="close($event)">OK</md-button>' +
							'</div>' +
						'</md-toast>',
					hideDelay: 0,
					controllerAs: 'toast',
					bindToController: true,
					position: 'bottom right',
					controller: 'defaultToastController'
				};
			}
		}).addPreset('success', {
			argOption: 'textContent',
			methods: ['textContent'],
			options: function() {
				return {
					template:
						'<md-toast>' +
							'<div class="md-toast-content">' +
								'<md-icon style="color:#fff;margin-right:6px;">done</md-icon> ' +
								'<span ng-bind="toast.textContent"></span> ' +
								'<md-button class="md-highlight" ng-click="close($event)">OK</md-button>' +
							'</div>' +
						'</md-toast>',
					hideDelay: 5000,
					controllerAs: 'toast',
					bindToController: true,
					position: 'bottom right',
					controller: 'defaultToastController'
				};
			}
		});
}).constant('API_JSONRPC_PATH', '/api_jsonrpc.php')
.filter('htmlTrusted', ['$sce', function($sce){
	return function(text){ return $sce.trustAsHtml(text); };
}]).filter('urlTrusted', ['$sce', function($sce){
	return function(url){ return $sce.trustAsResourceUrl(url); };
}]).directive('hcChart', function ($timeout){
	return {
		restrict: 'E',
		template: '<div></div>',
		scope: {
			chart: '=data'
		},
		link: function (scope, element){
			var chart = Highcharts.chart(element[0],scope.chart.options);
			$timeout(function(){
				chart.reflow();
			}, 100);
			scope.$watch('chart.options', function(newval){
				console.log("UPDATED OPTIONS");
				chart.update(newval);
				chart.reflow();
			}, true);
		}
	};
}).controller('defaultToastController', function($scope, $mdToast){
	this.$onInit = function() {
		var self = this;
		$scope.$watch(
			function(){ try { return activeToastContent; } catch(err){return null;} },
			function(){ try { self.content = activeToastContent; } catch(err){self.content = null;} }
		);
	};
	$scope.close = function(ev){
		$mdToast.hide();
	};
}).factory('nodusAPI', function($resource, $window, $cookies, API_JSONRPC_PATH){
	return {
		main: $resource($cookies.get('server-url') + API_JSONRPC_PATH, {}, {
				call: { 
					method:'POST', hasBody:true, headers:{'Content-Type':'application/json-rpc'},
					interceptor: {
						response: function(resp){
							$cookies.put('api-seq', parseInt($cookies.get('api-call-seq')?$cookies.get('api-seq'):'1')+1);
							console.log(resp);
							/*if(resp.status!=200){for(var key in $cookies.getAll()){$cookies.remove(key);}$window.location.href="/";return {};}*/
							return resp.resource;
						}
					}
				}
		}), do_logout: function(){
				for(var key in $cookies.getAll())
					$cookies.remove(key);
				$window.location.href = "/";
		}
	};
}).factory('sharedObj', function($cookies, $mdDialog){
	return {
		get_formatted_request: function(method, params){
			return {
				jsonrpc: "2.0",
				method: method,
				params: params,
				id: $cookies.get('api-call-seq'),
				auth: $cookies.get('api-token')
			};
		}, calculate_age: function(date1){
			var now = (new Date()).getTime();
			var total = parseInt( ( now - date1 ) / 1000 );
			if(total<60)
				return total + 's';
			else if(total<3600)
				return parseInt(total/60) + 'm';
			else
				return parseInt(total/3600) + 'h ' + (parseInt( (total%3600) / 60 )) + 'm';
		}, config_file: {
			dashboard: {
				main_chart: {
					options: {
						chart: { type:'pie' },
						plotOptions: { pie: { shadow: false, center: ['50%', '50%'] } },
						title: {text:'ITENS MONITORADOS'},
						credits: false,
						series: [{
							name: 'Links',
							colorByPoint: true,
							data: []
						}]
					}, data: [
						{
							method: 'trigger.get',
							display_name: 'Normal - Falha',
							problem: true,
							params: {
								output: "triggerid",
								filter: {value:1,priority:4,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						},{
							method: 'trigger.get',
							display_name: 'Normal - OK',
							problem: false,
							params: {
								output: "triggerid",
								filter: {value:0,priority:4,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						},{
							method: 'trigger.get',
							display_name: 'VIP - Falha',
							problem: true,
							params: {
								output: "triggerid",
								filter: {value:1,priority:5,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						},{
							method: 'trigger.get',
							display_name: 'VIP - OK',
							problem: false,
							params: {
								output: "triggerid",
								filter: {value:0,priority:5,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						}
					]
				}
			}, map: {
				main: {
					data: [
						{
							method: 'host.get',
							params: {
								output: ["name"],
								selectInventory: [
									"site_address_a", 
									"poc_1_notes",
									"site_city",
									"site_state",
									"location_lat",
									"location_lon"
								]
							}
						}
					]
				}
			}, tickets: {
				main_list: {
					config: {
						labels: {
							5: 'VIP',
							4: 'ALTO - P1',
							3: 'MEDIO - P2',
							2: 'BAIXO - P3'
						}, labels_order: [5,4,3,2]
					}, data: [
						{
							method: 'trigger.get',
							params: {
								output: ["triggerid", "description", "priority"],
								filter: {value:1,status:0},
								monitored: 1,
								maintenance: false,
								selectHosts: ["hostid"],
								selectLastEvent: ["clock"],
								expandDescription: 1,
								search: {description:"Link down"},
								sortfield: "priority",
								sortorder: "DESC"
							}
						}
					]
				}, host_details: {
					data: [
						{
							method: 'host.get',
							params: {
								output: ["name"],
								filter: {hostid:[]},
								selectInventory: [
									"site_address_a", 
									"poc_1_notes",
									"site_city",
									"site_state"
								]
							}
						}
					]					
				}
			}, items: {
				charts: {
					options: [
						{
							chart: { type:'pie' },
							plotOptions: { pie: { shadow: false, center: ['50%', '50%'] } },
							title: {text:'MPLS'},
							credits: false,
							series: [{
								name: 'Links',
								colorByPoint: true,
								data: []
							}]
						},{
							chart: { type:'pie' },
							plotOptions: { pie: { shadow: false, center: ['50%', '50%'] } },
							title: {text:'INTERNET'},
							credits: false,
							series: [{
								name: 'Links',
								colorByPoint: true,
								data: []
							}]
						},{
							chart: { type:'pie' },
							plotOptions: { pie: { shadow: false, center: ['50%', '50%'] } },
							title: {text:'ADSL'},
							credits: false,
							series: [{
								name: 'Links',
								colorByPoint: true,
								data: []
							}]
						}
					], data: [
						{
							method: 'trigger.get',
							params: {
								output: ["triggerid"],
								filter: {value:1,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						},{
							method: 'trigger.get',
							params: {
								output: ["triggerid"],
								filter: {value:1,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						},{
							method: 'trigger.get',
							params: {
								output: ["triggerid"],
								filter: {value:1,status:0},
								monitored: 1,
								maintenance: false,
								search: {description:"Link down"}
							}
						}
					]
				}
			}
		}
	};
}).controller('loginController', function($scope, $window, $cookies, $http, API_JSONRPC_PATH){

	$scope.login = {
		is_validating: false,
		url: 'https://monit.o2b.io',
		email: null,
		passwd: null
	};
	
	$scope.dologin = function(){
		$scope.currenterror = null;
		$scope.login.is_validating = true;
		$cookies.put('server-url', $scope.login.url);
		$cookies.put('api-call-seq', 1);
		$http({
			method: 'POST',
			url: $scope.login.url + API_JSONRPC_PATH,
			data: JSON.stringify({
				"jsonrpc": "2.0",
				"method": "user.login",
				"params": {
					"user": $scope.login.login,
					"password": $scope.login.passwd
				},
				"id": 1,
				"auth": null
			}), headers: {
				'Content-Type': 'application/json-rpc'
			}
		}).then(function(resp){
			if((resp.status==200)&&(resp.data)&&(resp.data.result)){
				$cookies.put('api-token', resp.data.result);
				$window.location.href = "/home.html#!/dashboard";
			} else {
				$scope.login.is_validating = false;
				$scope.currenterror = "Usuário ou senha inválidos";
			}
		}, function(resp){
			$scope.login.is_validating = false;
			$scope.currenterror = "Usuário ou senha inválidos";
			console.log(resp);
		});
	};
	
}).controller('menuController', function($scope, $mdSidenav, sharedObj, nodusAPI){

	$scope.sharedObj = sharedObj;
	$scope.current_menu = null;
	$scope.menu_list = [
		{href:'dashboard', icon:'apps', label:'DASHBOARD', clickFn: function(){ $scope.hide_menu(); }},
		{href:'tickets', icon:'warning', label:'PROBLEMAS', clickFn: function(){ $scope.hide_menu(); }},
		{href:'itens', icon:'pie_chart', label:'DISPOSITIVOS', clickFn: function(){ $scope.hide_menu(); }},
		{href:'mapa', icon:'map', label:'MAPA', clickFn: function(){ $scope.hide_menu(); }},
//		{href:'sobre', icon:'help', label:'SOBRE', clickFn: function(){ $scope.hide_menu(); }},
		{href:'#', icon:'power_settings_new', label:'SAIR', clickFn: function(ev){ $scope.do_logout(ev); }}
	];
	
	$scope.do_logout = function(ev){
		try{ ev.preventDefault(); } catch(err){}
		nodusAPI.do_logout();
	};
	$scope.is_current_page = function(menu){
		if((!$scope.current_menu)||(!$scope.current_menu.$$route))
			return false;
		var patt = new RegExp("^\/"+menu.href);
		return patt.test($scope.current_menu.$$route.originalPath);
	};
	
	$scope.hide_menu = function(){
		$mdSidenav('left-menu').close();
	};
	
	$scope.$on('$routeChangeStart', function(ev, next, current){
		$scope.current_menu = next;
	});
	
}).controller('mainBarController', function($scope, $mdSidenav, $mdMedia, sharedObj){

	$scope.sharedObj = sharedObj;

	$scope.show_menu_icon = function(){
		return $mdMedia('xs')||$mdMedia('sm');
	};
	$scope.toggle_menu = function(){
		return $mdSidenav('left-menu').toggle();
	};

}).controller('dashboardController', function($scope, $mdToast, nodusAPI, sharedObj, $timeout, $window){

	$scope.list = null;
	$scope.http_status = {
		is_loading_mainchart: false
	};
	$scope.main_chart_totals = {
		ok: null,
		problem: null
	};
	
	sharedObj.breadcrumb = [
		{label: 'Dashboard', url: null}
	];
	sharedObj.page_actions = [
		{
			icon: 'refresh',
			label: 'Recarregar',
			click: function(ev){
				$scope.get_dados();
			}
		}
	];
	
	$scope.goto_tickets = function(){
		$window.location.href = "#!/tickets";
	};
	
	$scope.mainchart = {
		options: sharedObj.config_file.dashboard.main_chart.options	
	};

	$scope.mainchart.options.series[0].data = [];
	for(var idx in sharedObj.config_file.dashboard.main_chart.data){
		$scope.mainchart.options.series[0].data.push({
			name: sharedObj.config_file.dashboard.main_chart.data[idx].display_name,
			y: 0
		});
	}
		
	var requests_finished = 0;
	$scope.get_dados = function(){

		if($scope.http_status.is_loading_mainchart)
			return ;

		$scope.http_status.is_loading_mainchart = true;
		requests_finished = 0;
		$scope.main_chart_totals.ok = null;
		$scope.main_chart_totals.problem = null;

		$mdToast.show($mdToast.loading('Aguarde... carregando dados.'));
		
		for(var idx in sharedObj.config_file.dashboard.main_chart.data){
			
			(function(idxc){
				
				var req = sharedObj.config_file.dashboard.main_chart.data[idxc];
				$scope.mainchart.options.series[0].data[idxc].y = 0;
				nodusAPI.main.call(null, sharedObj.get_formatted_request(req.method, req.params), 
				
				function(resp){
					
					requests_finished++;
					$scope.mainchart.options.series[0].data[idxc].y = resp.result.length;
					
					if(req.problem){
						if(!$scope.main_chart_totals.problem)
							$scope.main_chart_totals.problem = 0;
						$scope.main_chart_totals.problem += resp.result.length;
					} else {
						if(!$scope.main_chart_totals.ok)
							$scope.main_chart_totals.ok = 0;
						$scope.main_chart_totals.ok += resp.result.length;
					}
					
					if(requests_finished>=sharedObj.config_file.dashboard.main_chart.data.length){
						$scope.http_status.is_loading_mainchart = false;
						$mdToast.hide();
					}
					
				}, function(){
					requests_finished++;
					if(requests_finished>=sharedObj.config_file.dashboard.main_chart.data.length){
						$scope.http_status.is_loading_mainchart = false;
						$mdToast.hide();
					}
				});
				
			})(idx);
			
		}

	};
	
	$scope.get_dados();

}).controller('mapaController', function($scope, NgMap, sharedObj, nodusAPI, $mdToast, $filter){

	sharedObj.breadcrumb = [
		{label: 'Mapa', url: null}
	];
	sharedObj.page_actions = [
		{
			icon: 'refresh',
			label: 'Recarregar',
			click: function(ev){
				$scope.get_pins();
			}
		}
	];
	$scope.map = null;
	$scope.oms = null;
	$scope.infowin = null;
	$scope.is_loading = false;
	$scope.list_pins = [];
	$scope.current_pin = null;
	var requests_finished = 0;
	$scope.problem_list = [];
	
	$scope.get_pins = function(){

		if($scope.is_loading)
			return ;
		
		$scope.is_loading = true;
		$mdToast.show($mdToast.loading('Aguarde... carregando dados.'));
		
		if($scope.list_pins.length){
			for(var idx in $scope.list_pins){
				$scope.oms.removeMarker($scope.list_pins[idx].marker);
			}
		}
		
		$scope.list_pins = [];
		$scope.problem_list = [];
		var temp_list_pins = [];
		var problem_list_idx = [];
		
		var req = sharedObj.config_file.tickets.main_list.data[0];
		nodusAPI.main.call(null, sharedObj.get_formatted_request(req.method, req.params), 
		function(resp){
			
			$scope.problem_list = resp.result;
			for(var i=0; i<resp.result.length; i++)
				problem_list_idx.push(resp.result[i].hosts[0].hostid);
						
			for(var idx in sharedObj.config_file.map.main.data){

				(function(idxc){

					var req = sharedObj.config_file.map.main.data[idxc];
					nodusAPI.main.call(null, sharedObj.get_formatted_request(req.method, req.params), 

					function(resp){

						requests_finished++;
						temp_list_pins = temp_list_pins.concat(resp.result);

						if(requests_finished>=sharedObj.config_file.map.main.data.length){

							$scope.is_loading = false;
							$mdToast.hide();

							var idxfound;
							for(var idxlist in temp_list_pins){
								temp_list_pins[idxlist].problem = null;
								idxfound = problem_list_idx.indexOf(temp_list_pins[idxlist].hostid);
								if(idxfound<0) continue;
								temp_list_pins[idxlist].problem = $scope.problem_list[idxfound];
								temp_list_pins[idxlist].problem.lastEvent.clock = parseInt(temp_list_pins[idxlist].problem.lastEvent.clock) * 1000;
							}

							$scope.list_pins = temp_list_pins;
							var bounds = new google.maps.LatLngBounds();
							for (var i=0; i<$scope.list_pins.length; i++) {
								bounds.extend(new google.maps.LatLng($scope.list_pins[i].inventory.location_lat, $scope.list_pins[i].inventory.location_lon));
							}

							$scope.map.setCenter(bounds.getCenter());
							$scope.map.fitBounds(bounds);
							for(var idxpin in $scope.list_pins){
								(function(myidxpin){
									$scope.list_pins[myidxpin].marker = new google.maps.Marker({
										position: {lat:parseFloat($scope.list_pins[myidxpin].inventory.location_lat), lng:parseFloat($scope.list_pins[myidxpin].inventory.location_lon)},
										draggable: false,
										animation: google.maps.Animation.DROP,
										icon: $scope.list_pins[myidxpin].problem?'https://maps.google.com/mapfiles/ms/icons/red-dot.png':'https://maps.google.com/mapfiles/ms/icons/green-dot.png',
										zIndex: $scope.list_pins[myidxpin].problem?9999:1
									});
									$scope.oms.addMarker($scope.list_pins[myidxpin].marker, function(ev){
										$scope.openinfo(ev, $scope.list_pins[myidxpin]);
									});
								})(idxpin);
							}
//		<!--marker ng-repeat="pin in list_pins" id="pin_{{pin.hostid}}" icon="{{pin.problem?'https://maps.google.com/mapfiles/ms/icons/red-dot.png':'https://maps.google.com/mapfiles/ms/icons/green-dot.png'}}" animation="DROP" position="[{{pin.inventory.location_lat}}, {{pin.inventory.location_lon}}]" opacity="{{!current_pin||current_pin==pin?1:0.3}}" on-click="openinfo(event,pin)"></marker-->

						}

					}, function(){
						requests_finished++;
						if(requests_finished>=sharedObj.config_file.map.main.data.length){
							$scope.is_loading = false;
							$mdToast.hide();
						}
					});

				})(idx);

			}
			
		}, function(resp){
			$scope.is_loading = false;
			$mdToast.hide();
		});
		
	};
	
	$mdToast.show($mdToast.loading('Aguarde... carregando dados.'));
	NgMap.getMap().then(function(map){
		$scope.map = map;
		$scope.oms = new OverlappingMarkerSpiderfier($scope.map, {
			markersWontMove: true,
			markersWontHide: true,
			basicFormatEvents: true,
			keepSpiderfied: true
		});
		$scope.oms.addListener('spiderfy', function(spiderfied, unspiderfied) {
			for(var idx in unspiderfied){
				unspiderfied[idx].setOpacity(0.2);
			}
		});
		$scope.oms.addListener('unspiderfy', function(spiderfied, unspiderfied) {
			for(var idx in unspiderfied){
				unspiderfied[idx].setOpacity(1);
			}
			$scope.infowin.close();
		});
		$scope.infowin = new google.maps.InfoWindow();
		$scope.get_pins();
	});
		
	$scope.openinfo = function(ev, pin){
		
		$scope.current_pin = pin;
		
		var content = '<div style="max-width:280px;">';
		content += '<strong style="color:'+(pin.problem?'#c00':'auto')+';">';
		
		content += '<span>'+pin.name+'</span>';
		content += '</strong>';
		content += '<div style="font-size:110%;padding-top:7px;">Endereço</div><div style="font-size:90%;padding:5px 0;">'+pin.inventory.site_address_a+'</div><md-divider></md-divider>';
		content += '<div style="font-size:110%;padding-top:7px;">Localidade</div><div style="font-size:90%;padding:5px 0;">'+pin.inventory.site_city+' / '+pin.inventory.site_state+'</div><md-divider></md-divider>';
		
		
		if(pin.problem){
			content += '<div style="font-size:110%;padding-top:7px;">Prioridade</div><div style="font-size:90%;padding:5px 0;">'+pin.problem.priority+'</div><md-divider></md-divider>';
			content += '<div style="font-size:110%;padding-top:7px;">Data do problema</div><div style="font-size:90%;padding:5px 0;">'+$filter('date')(pin.problem.lastEvent.clock,'dd/MM/yyyy HH:mm')+'</div><md-divider></md-divider>';
			content += '<div style="font-size:110%;padding-top:7px;">Idade do problema</div><div style="font-size:90%;padding:5px 0;">'+sharedObj.calculate_age(pin.problem.lastEvent.clock)+'</div><md-divider></md-divider>';
		}
		
		content += '<div style="font-size:110%;padding-top:7px;">Contatos</div><div style="font-size:90%;padding:5px 0;">'+(pin.inventory.poc_1_notes?pin.inventory.poc_1_notes:'----------')+'</div><md-divider></md-divider>';
		content += '</div>';
		$scope.infowin.setContent(content);
		$scope.infowin.open($scope.map, pin.marker);
		
	};
	
	$scope.clear_current_pin = function(){
		$scope.current_pin = null;
	};
		
}).controller('ticketsController', function($scope, sharedObj, $mdToast, nodusAPI, $mdDialog){

	sharedObj.breadcrumb = [
		{label: 'Problemas', url: null}
	];
	sharedObj.page_actions = [
		{
			icon: 'refresh',
			label: 'Recarregar',
			click: function(ev){
				$scope.get_tickets();
			}
		}
	];
	$scope.config = {
		search_pattern: null,
		is_loading: false
	};
	$scope.lista = [];
	$scope.section_list = {
		labels: sharedObj.config_file.tickets.main_list.config.labels,
		order: sharedObj.config_file.tickets.main_list.config.labels_order
	};

	$scope.get_tickets = function(){

		$scope.lista = [];
		$scope.selecionados = {};
		$scope.config.is_loading = true;
		$mdToast.show($mdToast.loading('Aguarde... carregando os problemas.'));
		
		var req = sharedObj.config_file.tickets.main_list.data[0];
		
		nodusAPI.main.call(null, sharedObj.get_formatted_request(req.method, req.params), 
		function(resp){
			$mdToast.hide();
			$scope.lista = resp.result;
			for(var idx in $scope.lista)
				$scope.lista[idx].lastEvent.clock = parseInt($scope.lista[idx].lastEvent.clock) * 1000;
			$scope.config.is_loading = false;
		}, function(resp){
			$mdToast.hide();
			$scope.config.is_loading = false;
		});
		
	};
	$scope.get_tickets();
	
	$scope.open_ticket = function(ev, ticket){
		$mdDialog.show({
			controller: 'ticketDetailsDialogController',
			templateUrl: '/template-page/dialog-ticket-details.html',
			parent: angular.element(document.body),
			locals: {ticket:ticket},
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: true
		})
		.then(function(answer) {
		}, function() {
		});
	};
		
}).controller('ticketDetailsDialogController', function($scope, sharedObj, ticket, $mdToast, nodusAPI, $mdDialog, $timeout){

	$scope.dados = null;
	$scope.is_loading = false;
	$scope.cancel = function(){
		$mdDialog.hide();
	};
	$scope.ticket = ticket;
	$scope.calculate_age = sharedObj.calculate_age;
	
	$scope.load_ticket = function(){

		$scope.dados = null;
		$mdToast.show($mdToast.loading('Aguarde... carregando detalhes.'));
		
		var req = sharedObj.config_file.tickets.host_details.data[0];
		req.params.filter.hostid = [ticket.hosts[0].hostid];

		nodusAPI.main.call(null, sharedObj.get_formatted_request(req.method, req.params), function(resp){
			$mdToast.hide();
			$scope.dados = resp.result[0];
			$scope.is_loading = false;
		}, function(resp){
			$mdToast.hide();
			$scope.is_loading = false;
		});
		
	};
	
	$scope.load_ticket();

}).controller('itensController', function($scope, sharedObj, nodusAPI, $mdToast){

	sharedObj.breadcrumb = [
		{label: 'Dispositivos', url: null}
	];
	sharedObj.page_actions = [
		{
			icon: 'refresh',
			label: 'Recarregar',
			click: function(ev){
				$scope.load_data();
			}
		}
	];
	$scope.is_loading = false;
	
	$scope.list = [];
	for(var idx in sharedObj.config_file.items.charts.options)
		$scope.list.push({options:sharedObj.config_file.items.charts.options[idx]});
	
	var requests_finished = 0;
	$scope.load_data = function(){
		
		if($scope.is_loading)
			return ;
		
		for(var idx in $scope.list)
			$scope.list[idx].options.series[0].data = [];
		
		$scope.is_loading = true;
		requests_finished = 0;
		$mdToast.show($mdToast.loading('Aguarde... carregando dados.'));

		for(var idx in sharedObj.config_file.items.charts.data){

			(function(idxc){

				var req = sharedObj.config_file.items.charts.data[idxc];
				nodusAPI.main.call(null, sharedObj.get_formatted_request(req.method, req.params), 

				function(resp){

					requests_finished++;

					$scope.list[idxc].options.series[0].data.push({name:"a", y:resp.result.length});

					if(requests_finished>=sharedObj.config_file.items.charts.data.length){
						$scope.is_loading = false;
						$mdToast.hide();
					}

				}, function(){
					requests_finished++;
					if(requests_finished>=sharedObj.config_file.items.charts.data.length){
						$scope.is_loading = false;
						$mdToast.hide();
					}
				});

			})(idx);

		}
		
	};

	$scope.load_data();

});