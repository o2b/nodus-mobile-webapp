/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.
 */
 // [START initialize_firebase_in_sw]
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here, other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/4.9.0/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/4.9.0/firebase-messaging.js');
 // Initialize the Firebase app in the service worker by passing in the
 // messagingSenderId.
	firebase.initializeApp({
    apiKey: "AIzaSyD_bON4ocmTwXGk0U4WfqrXDZiJYqw96_I",
    authDomain: "nodus-mobile.firebaseapp.com",
    databaseURL: "https://nodus-mobile.firebaseio.com",
    projectId: "nodus-mobile",
    storageBucket: "nodus-mobile.appspot.com",
    messagingSenderId: "716967315570"
	});
 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 // [END initialize_firebase_in_sw]

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
console.log('registering background message handler from Service Worker...');
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = payload.data.notif_title;
  const notificationOptions = {
    body: payload.data.notif_body,
	icon: '/firebase-logo.png',
	click_action: 'https://nodus-mobile.firebaseapp.com'
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
// [END background_handler]